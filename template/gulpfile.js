/* jshint node: true */

'use strict';

var gulp = require('gulp'),
    path = require('path'),
    taskLoader = require('gulp-task-loader'),
    runSequence = require('run-sequence'),
    browserSync = require('browser-sync').create();

taskLoader({
  dir: path.relative('.', 'gulp/tasks'),
  path: path,
  watch: require('gulp-watch'),
  runSequence: runSequence,
  config: require(path.resolve(__dirname, 'gulp/config.js')),
  vendors: require(path.resolve(__dirname, 'gulp/vendors.js')),
  pkg: require(path.resolve(__dirname, 'package.json')),
  browserSync: browserSync
});

gulp.task('design', ['design:clean'], function() {
  console.log('----- Starting to export design ------');
  runSequence(['design:export'],
    function() {
      console.log('----- Design has been exported successfully ------');
    });
});


gulp.task('compile', ['compile:clean'], function() {
  console.log('----- Starting to compile project ------');
  runSequence(['compile:iconfonts', 'compile:favicons'],
  ['compile:data', 'compile:pages', 'compile:webfonts', 'compile:images', 'compile:styles', 'compile:plugins', 'compile:modernizr', 'compile:scripts'],
    function() {
      console.log('----- Project has been compiled successfully ------');
    });
});

gulp.task('test', function() {
  console.log('----- Starting to test project ------');
  runSequence(['test:scripts', 'test:pages'], ['test:lint','test:accessibility','test:run'],
    function() {
      console.log('----- Project has been tested ------');
    });
});


gulp.task('build', ['build:clean'], function() {
  console.log('----- Starting to build project ------');
  runSequence(['build:data', 'build:pages', 'build:styles', 'build:fonts', 'build:images', 'build:favicons', 'build:scripts', 'build:misc'],
    function() {
      console.log('----- Project has been built successfully ------');
    });
});

gulp.task('integrate', ['integrate:clean'], function() {
  console.log('----- Starting to integrate project ------');
  runSequence(['integrate:data', 'integrate:styles', 'integrate:fonts', 'integrate:images', 'integrate:favicons', 'integrate:scripts', 'integrate:misc'],
    function() {
      console.log('----- Project has been integrated successfully ------');
    });
});

gulp.task('pack', ['package:clean'], function() {
  console.log('----- Starting to package project ------');
  runSequence(['package:executables'],
    function() {
      console.log('----- Project has been packaged successfully ------');
    });
});


gulp.task('watch', function() {
  console.log('----- Starting to watch project ------');
  runSequence(['watch:data', 'watch:pages', 'watch:styles', 'watch:fonts', 'watch:misc', 'watch:images', 'watch:scripts'],
    function() {
      console.log('----- Project is under watch ------');
    });
});

gulp.task('run',function() {
  console.log('----- Starting to run proejct ------');
  runSequence('serve:server', ['serve:browsersync', 'watch'],
    function() {
      console.log('----- Project is running ------');
    });
});

gulp.task('default', ['compile']);

gulp.task('clean', ['compile:clean', 'build:clean', 'integrate:clean']);
