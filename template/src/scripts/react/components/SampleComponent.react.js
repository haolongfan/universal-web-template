class SampleComponent extends React.Component{

  constructor(props) {
    super(props);

    this.state = {

    };

  }

  componentWillMount(){
    sampleStore.listen(this.onSampleUpdate.bind(this));
  }

  componentWillUnmount(){
    sampleStore.unlisten(this.onSampleUpdate.bind(this));
  }

  onSampleUpdate() {
    this.setState({
      sample: sampleStore.getState().sample
    });
  }


  render() {

    return (
      <div>

      </div>
    );
  }
};
