let ReactRouter = window.ReactRouter,
    Router = ReactRouter.Router,
    IndexRoute = ReactRouter.IndexRoute,
    browserHistory = ReactRouter.browserHistory,
    useRouterHistory = ReactRouter.useRouterHistory,
    Route = ReactRouter.Route;

let History = window.History,
    createHashHistory = History.createHashHistory;

const appHistory = useRouterHistory(createHashHistory)({ queryKey: false })

const routes = (
  <Route path="/">
    <IndexRoute component={SampleComponent} />
    <Route path="page" component={SampleComponent} />
  </Route>
);

ReactDOM.render((<Router routes={routes} history={appHistory}/>), document.getElementById('page'));

ReactDOM.render(<SampleComponent />, document.getElementById('sample'));
