class SampleStore {

  constructor() {

    this.bindActions(sampleActions);

    this.state = {
      sample: null
    };

  }

}
