'use strict';

var path = require('path');

module.exports = function (str, start, len) {

    if (str.length > len) {
        var new_str = str.substr(0, len+1);

        while (new_str.length) {
            var ch = new_str.substr(-1);
            new_str = new_str.substr(0, -1);

            if (ch == ' ') {
                break;
            }
        }

        if ( new_str == '' ) {
            new_str = str.substr(start, len);
        }

        return new_str.trim();
    }
    return str.trim();

}
