'use strict';

var path = require('path');

module.exports = function(lvalue, operator, rvalue, opts) {

  if (arguments.length < 4)
    throw new Error("Handlerbars Helper 'iff' needs 3 parameters");


  var result = false;

  switch(operator) {
     case '==':
         result = lvalue == rvalue;
         break;
     case '!=':
         result = lvalue != rvalue;
         break;
     case '>':
         result = lvalue > rvalue;
         break;
     case '>=':
         result = lvalue >= rvalue;
         break;
     case '<':
         result = lvalue < rvalue;
         break;
     case '<=':
         result = lvalue <= rvalue;
         break;
     case 'typeof':
         result = typeof lvalue == rvalue;
         break;
     default:
         throw "Unknown operator " + operator;
  }

  if (result) {
      return opts.fn(this);
  } else {
      return opts.inverse(this);
  }

};
