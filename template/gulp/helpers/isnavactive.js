'use strict';

var path = require('path');

module.exports = function(opts) {

  var result = false;

  var currentPageTitle = "",
      parentPageTitle = "",
      currentNavItemTitle = "",
      breadcrumbItemTitle = "";


  if(opts.data.root && opts.data.root._page) {

    var currentPage = opts.data.root._page;

    currentNavItemTitle = opts.data.key;
    currentPageTitle = currentPage.title;

    result = currentPageTitle == currentNavItemTitle;

    if(!result && currentPage.breadcrumb && currentPage.breadcrumb.length > 1) {
      breadcrumbItemTitle = currentPage.breadcrumb[1].text;
      result = currentNavItemTitle == breadcrumbItemTitle;
    }

  }

  if (result) {
      return opts.fn(this);
  } else {
      return opts.inverse(this);
  }

};
