/* jshint node: true */

'use strict';

var path = require('path'),
    pkg = require('../package.json');

var rootDir = path.resolve('.'),
    appDir = path.resolve(__dirname, '../app'),
    helperDir = path.resolve(__dirname, '../gulp/helpers'),
    sourceDir = path.resolve(__dirname, '..' + pkg.path.source),
    compileDir = path.resolve(__dirname, '..' + pkg.path.compile.root),
    buildDir = path.resolve(__dirname, '..' + pkg.path.build.root),
    integrateDir = path.resolve(__dirname, '..' + pkg.path.integrate.root),
    integrateThemeDir = path.resolve(__dirname, '..' + pkg.path.integrate.root + pkg.path.integrate.theme),
    packageDir = path.resolve(__dirname, '..' + pkg.path.package.root);

module.exports = {

  root: rootDir,
  helpers: helperDir,
  app: appDir,

  // Server
  server: {
    admin_port: pkg.server.admin_port, // port number for admin
    site_port: pkg.server.site_port // site port
  },

  // Source
  source: {
    src: sourceDir,
    design: {
      sketch: sourceDir + '/design/sketch',
      slices: sourceDir + '/design/slices'
    },
    data: sourceDir + '/data',
    pages: sourceDir + '/pages',
    scripts: sourceDir + '/scripts',
    sass: sourceDir + '/sass',
    fonts: sourceDir + '/fonts',
    images: sourceDir + '/images',
    icons: sourceDir + '/images/icons',
    favicons: sourceDir + '/favicons',
    misc: {
      build: sourceDir + '/misc/build',
      integrate: sourceDir + '/misc/integrate'
    }
  },

  // Scripts
  script: {
    custom: sourceDir + '/scripts/custom',
    react: sourceDir + '/scripts/react',
    polyfills: sourceDir + '/scripts/polyfills',
    vendors: sourceDir + '/scripts/vendors',
    plugins: sourceDir + '/scripts/plugins',
    tests: sourceDir + '/scripts/tests'
  },

  // Compile Template
  compile: {
    root: compileDir,
    data: compileDir + pkg.path.compile.data,
    styles: compileDir + pkg.path.compile.styles,
    fonts: compileDir + pkg.path.compile.fonts,
    images: compileDir + pkg.path.compile.images,
    scripts: compileDir + pkg.path.compile.scripts,
    plugins: compileDir + pkg.path.compile.plugins,
    reports: compileDir + pkg.path.compile.reports
  },

  // Build Template
  build: {
    root: buildDir,
    data: buildDir + pkg.path.build.data,
    styles: buildDir + pkg.path.build.styles,
    fonts: buildDir + pkg.path.build.fonts,
    images: buildDir + pkg.path.build.images,
    scripts: buildDir + pkg.path.build.scripts
  },

  // Integrate template within CMS
  integrate: {
    root: integrateDir,
    theme: integrateThemeDir,
    data: integrateThemeDir + pkg.path.integrate.data,
    styles: integrateThemeDir + pkg.path.integrate.styles,
    fonts: integrateThemeDir + pkg.path.integrate.fonts,
    images:integrateThemeDir + pkg.path.integrate.images,
    scripts: integrateThemeDir + pkg.path.integrate.scripts,
  },

  // App Outputs
  package: {
    root: packageDir,
    app: packageDir + pkg.path.package.app,
    cache: packageDir + pkg.path.package.cache,
  },


  // File type
  filetype: {
    fonts: '**/*.{eot,svg,ttf,woff,woff2}',
    images: '**/*.{jpg,svg,png,gif}',
    favicons: '/*.{png,xml,json,ico}'
  }

};
