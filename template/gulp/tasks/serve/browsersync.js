/* jshint node: true */

/*
  serve:browsersync Task
  Run project on browserSync
*/

'use strict';

module.exports = function() {

  var admin_port = this.opts.pkg.server.admin_port,
      site_port = this.opts.pkg.server.site_port;
  var apis = this.opts.pkg.apis;

  return this.opts.browserSync.init({
    ui: {
      port: admin_port
    },
    proxy: {
      target: "localhost:" + this.opts.pkg.server.site_port,
      ws: true
    },
    port: this.opts.pkg.server.proxied_port
  });

};
