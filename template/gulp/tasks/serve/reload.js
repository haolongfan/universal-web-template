/* jshint node: true */

/*
  serve:reload Task
  Reload project on browserSync
*/

'use strict';

module.exports = function() {

  var browserSync = this.opts.browserSync;

  if(browserSync)
  return browserSync.reload();

};
