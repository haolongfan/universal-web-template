/* jshint node: true */

/*
  serve:run Task
  Run project on node
*/

'use strict';

var nodemon = require('gulp-nodemon');
const BROWSER_SYNC_RELOAD_DELAY = 500;

module.exports = function(callback) {

  var started = false;
  var gulp = this.gulp;
  var browserSync = this.opts.browserSync;

  return nodemon({
    script: 'server.js',
    watch: 'dist/'
  })
  .on('start', function onStart() {
     // ensure start only got called once
     if (!started) {
       callback();
       started = true;
     }

   });
};
