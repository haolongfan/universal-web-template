/* jshint node: true */

/*
  release:clean Task
  remove the whole release directory
*/

'use strict';

var del = require('del');

module.exports = function() {

  return del([
    this.opts.config.package.root
  ]);

};
