/* jshint node: true */

/*
  release:executables Task
  package in desktop OS executables (.app, .exe, etc)
*/

'use strict';

var packager = require('electron-packager'),
    electron = require('gulp-electron');

module.exports = function() {

    var pkg = this.opts.pkg,
        projectName = pkg.name,
        appName = pkg.app.name,
        version = pkg.version;

    return this.gulp.src("")
      .pipe(electron({
          src: this.opts.config.build.root,
          packageJson: pkg,
          release: this.opts.config.package.app,
          cache: this.opts.config.package.cache,
          version: 'v1.4.4',
          packaging: false,
          platforms: ['darwin-x64'],
          platformResources: {
              darwin: {
                  CFBundleDisplayName: appName,
                  CFBundleIdentifier: projectName,
                  CFBundleName: appName,
                  CFBundleVersion: version,
                  icon: this.opts.config.build.root + '/favicon.ico'
              }
          }
      }))
      .pipe(this.gulp.dest(""));

};
