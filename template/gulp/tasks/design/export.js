/* jshint node: true */

/*
  sketch: export task
  Export Sketch assets
*/

'use strict';

var sketch = require('gulp-sketch');

module.exports = function(callback) {

  var source = this.opts.config.source.design.sketch,
      dest = this.opts.config.source.design.slices;

  return this.gulp.src([
    this.opts.path.join(source, '**/*.sketch')
  ])
  .pipe(sketch({
   export: 'slices',
   formats: 'png',
   saveForWeb: true,
   compact: true,
   clean: true
  }))
  .pipe(this.gulp.dest(dest));


};
