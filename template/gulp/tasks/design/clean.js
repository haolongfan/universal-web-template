/* jshint node: true */

/*
  build:clean Task
  Clean all design elements
*/

'use strict';

var del = require('del');

module.exports = function() {

  return del([
    this.opts.config.source.design.slices
  ]);

};
