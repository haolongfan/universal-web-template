/* jshint node: true */

/*
  cms:favicons Task
  Copy favicons files into CMS directory
*/

'use strict';

module.exports = function() {

  var source = this.opts.config.build.root,
      dest = this.opts.config.integrate.theme;

  return this.gulp.src([
    this.opts.path.join(source, this.opts.config.filetype.favicons)
  ])
  .pipe(this.gulp.dest(dest));

};
