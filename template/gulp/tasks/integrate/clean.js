/* jshint node: true */

/*
  cms:clean Task
  Clean all generated files from CMS directory
*/

'use strict';

var del = require('del');

module.exports = function() {

  return del([
    this.opts.config.integrate.styles,
    this.opts.config.integrate.scripts,
    this.opts.config.integrate.fonts,
    this.opts.config.integrate.images,
    this.opts.config.integrate.data,
    this.opts.config.theme + '/*.*'
  ]);

};
