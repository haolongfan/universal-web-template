/* jshint node: true */

/*
  cms:images Task
  Copy image files into CMS directory
*/

'use strict';

module.exports = function() {

  var source = this.opts.config.build.images,
      dest = this.opts.config.integrate.images;

  return this.gulp.src([
    this.opts.path.join(source, this.opts.config.filetype.images)
  ])
  .pipe(this.gulp.dest(dest));

};
