/* jshint node: true */

/*
  cms:scripts Task
  Copy javascript files into CMS directory
*/

'use strict';

module.exports = function() {

  var source = this.opts.config.build.scripts,
      dest = this.opts.config.integrate.scripts;

  return this.gulp.src([
    this.opts.path.join(source, '**/*.js')
  ])
  .pipe(this.gulp.dest(dest));

};
