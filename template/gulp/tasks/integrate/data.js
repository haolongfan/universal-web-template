/* jshint node: true */

/*
  cms:data Task
  Copy data files into CMS directory
*/

'use strict';

module.exports = function() {

  var source = this.opts.config.build.data,
      dest = this.opts.config.integrate.data;

  return this.gulp.src([
    this.opts.path.join(source, '**/*')
  ])
  .pipe(this.gulp.dest(dest));

};
