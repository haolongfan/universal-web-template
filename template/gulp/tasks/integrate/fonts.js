/* jshint node: true */

/*
  cms:fonts Task
  Copy font files into CMS directory
*/

'use strict';

module.exports = function() {

  var source = this.opts.config.build.fonts,
      dest = this.opts.config.integrate.fonts;

  return this.gulp.src([
    this.opts.path.join(source, this.opts.config.filetype.fonts)
  ])
  .pipe(this.gulp.dest(dest));

};
