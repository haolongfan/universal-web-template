/* jshint node: true */

/*
  cms:misc Task
  Copy misc files into CMS directory
*/

'use strict';

module.exports = function() {

  var source = this.opts.config.source.misc.integrate,
      dest = this.opts.config.integrate.root;

  return this.gulp.src([
    this.opts.path.join(source, '**/*')
  ])
  .pipe(this.gulp.dest(dest));

};
