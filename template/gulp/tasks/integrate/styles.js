/* jshint node: true */

/*
  cms:styles Task
  Copy css files into CMS directory
*/

'use strict';

module.exports = function() {

  var source = this.opts.config.build.styles,
      dest = this.opts.config.integrate.styles;

  return this.gulp.src([
    this.opts.path.join(source, '**/*.css')
  ])
  .pipe(this.gulp.dest(dest));

};
