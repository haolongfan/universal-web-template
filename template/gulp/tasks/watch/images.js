/* jshint node: true */

/*
  watch:images Task
  Watch changes of images files
*/

'use strict';

module.exports = function() {

  var gulp = this.gulp,
      runSequence = this.opts.runSequence;

  return this.opts.watch([
    this.opts.path.join(this.opts.config.source.images, this.opts.config.filetype.images)
  ], function(){
    runSequence('compile:images', ['serve:reload', 'build:images'], 'integrate:images');
  });

};
