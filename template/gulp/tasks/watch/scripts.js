/* jshint node: true */

/*
  watch:scripts Task
  Watch changes of javascript files
*/

'use strict';

module.exports = function() {

  var gulp = this.gulp,
      runSequence = this.opts.runSequence;

  return this.opts.watch([
    this.opts.path.join(this.opts.config.source.scripts, '**/*.js')
  ], function(){
    runSequence('compile:scripts', ['serve:reload', 'build:scripts'], 'integrate:scripts');
  });

};
