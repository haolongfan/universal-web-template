/* jshint node: true */

/*
  watch:misc Task
  Watch changes of misc files
*/

'use strict';

module.exports = function() {

  var gulp = this.gulp,
      runSequence = this.opts.runSequence;

  return this.opts.watch([
    this.opts.path.join(this.opts.config.source.misc.build, '**/*.*'),
    this.opts.path.join(this.opts.config.source.misc.integrate, '**/*.*')
  ], function(){
    runSequence(['build:misc', 'integrate:misc']);
  });

};
