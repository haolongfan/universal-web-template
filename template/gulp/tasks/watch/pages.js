/* jshint node: true */

/*
  watch:pages Task
  Watch changes of page template source files
*/

'use strict';

module.exports = function() {

  var gulp = this.gulp,
      runSequence = this.opts.runSequence;

  return this.opts.watch([
    this.opts.path.join(this.opts.config.source.pages, '**/*.{json,hbs}')
  ], function(){
    runSequence('compile:pages', ['serve:reload', 'build:pages']);
  });

};
