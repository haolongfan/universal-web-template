/* jshint node: true */

/*
  watch:fonts Task
  Watch changes of font files
*/

'use strict';

module.exports = function() {

  var gulp = this.gulp,
      runSequence = this.opts.runSequence;

  return this.opts.watch([
    this.opts.path.join(this.opts.config.source.fonts, this.opts.config.filetype.fonts)
  ], function(){
    runSequence('compile:webfonts', ['serve:reload', 'build:fonts'], 'integrate:fonts');
  });

};
