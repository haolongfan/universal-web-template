/* jshint node: true */

/*
  watch:styles Task
  Watch changes of scss files
*/

'use strict';

module.exports = function() {

  var gulp = this.gulp,
      runSequence = this.opts.runSequence;

  return this.opts.watch([
    this.opts.path.join(this.opts.config.source.sass, '**/*.scss')
  ], function(){
    runSequence('compile:styles', ['serve:reload', 'build:styles'], 'integrate:styles');
  });

};
