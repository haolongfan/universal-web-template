/* jshint node: true */

/*
  watch:data Task
  Watch changes of data files
*/

'use strict';

module.exports = function() {

  var gulp = this.gulp,
      runSequence = this.opts.runSequence;

  return this.opts.watch([
    this.opts.path.join(this.opts.config.source.data, '**/*.*')
  ], function(){
    runSequence('compile:data', ['serve:reload', 'build:data'], 'integrate:data');
  });

};
