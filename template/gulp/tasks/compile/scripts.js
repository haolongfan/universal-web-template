/* jshint node: true */

/*
  build:scrips Task
  Generate javascript files
*/

'use strict';

var concat = require('gulp-concat'),
    uglify = require('gulp-uglify'),
    babel = require('gulp-babel'),
    sourcemaps = require('gulp-sourcemaps'),
    plumber = require('gulp-plumber');

module.exports = function() {

  var reactEnabled = this.opts.pkg.modules.react,
      source = this.opts.config.script,
      dest = this.opts.config.compile.scripts;

  // core plugins
  this.gulp.src(this.opts.vendors.plugins.concat([
    this.opts.path.join(source.vendors, '/**/*.js'),
    this.opts.path.join(source.polyfills, '/**/*.js')
  ]))
  .pipe(plumber())
  .pipe(sourcemaps.init())
  .pipe(concat('core.js'))
  .pipe(this.gulp.dest(dest));

  if(reactEnabled) {

    // build react scripts
    return this.gulp.src([
      this.opts.path.join(source.custom, '/**/*.js'),
      this.opts.path.join(source.react, '/**/*.js')
    ])
    .pipe(plumber())
    .pipe(sourcemaps.init())
    .pipe(babel({
      presets: ['es2015', "react"]
    }))
    .pipe(concat('script.js'))
    .pipe(sourcemaps.write('/'))
    .pipe(this.gulp.dest(dest));

  }
  else {

    this.gulp.src([
      this.opts.path.join(source.custom, '/**/*.js')
    ])
    .pipe(plumber())
    .pipe(sourcemaps.init())
    .pipe(concat('script.js'))
    .pipe(sourcemaps.write('/'))
    .pipe(this.gulp.dest(dest));

  }

};
