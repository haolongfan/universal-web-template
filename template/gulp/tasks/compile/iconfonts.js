/* jshint node: true */

/*
  build:iconfonts Task
  Generate Icon Fonts and related styles
*/

'use strict';

var plumber = require('gulp-plumber'),
    iconfont = require('gulp-iconfont'),
    iconfontCss = require('gulp-iconfont-css');

module.exports = function() {

  var fontName = 'Icons',
      source = this.opts.config.source.icons,
      dest = this.opts.config.source.fonts;

  return this.gulp.src([
    this.opts.path.join(source, '**/*.svg')
  ])
  .pipe(plumber())
  .pipe(iconfontCss({
    fontName: fontName,
    targetPath: this.opts.path.relative(dest, 'src/sass/generic/_iconfont.scss'),
    fontPath: '../fonts/'
  }))
  .pipe(iconfont({
    fontName: fontName,
    formats: ['ttf'],
    normalize: true
  }))
  .pipe(this.gulp.dest(dest));

};
