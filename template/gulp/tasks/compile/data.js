/* jshint node: true */

/*
  build:data Task
  Copy data files into build directory
*/

'use strict';

module.exports = function() {

  var source = this.opts.config.source.data,
      dest = this.opts.config.compile.data;

  return this.gulp.src([
    this.opts.path.join(source, '**/*')
  ])
  .pipe(this.gulp.dest(dest));

};
