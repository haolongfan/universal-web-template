/* jshint node: true */

/*
  build:favicons Task
  Generate favicons
*/

'use strict';

var gutil = require('gulp-util'),
    plumber = require('gulp-plumber'),
    favicons = require("gulp-favicons");

module.exports = function() {

    var pkg = this.opts.pkg,
        source = this.opts.config.source.favicons,
        dest = this.opts.config.compile.root;

    return this.gulp.src([
      this.opts.path.join(source, '/icon.png')
    ])
    .pipe(plumber())
    .pipe(favicons({
          appName: pkg.app.name,
          appDescription: pkg.app.description,
          developerName: pkg.author,
          path: "/",
          url: "/",
          display: "standalone",
          orientation: "portrait",
          version: 1.0,
          icons: {
            android: true,              // Create Android homescreen icon. `boolean`
            appleIcon: true,            // Create Apple touch icons. `boolean`
            appleStartup: false,         // Create Apple startup images. `boolean`
            coast: false,                // Create Opera Coast icon. `boolean`
            favicons: true,             // Create regular favicons. `boolean`
            firefox: false,              // Create Firefox OS icons. `boolean`
            opengraph: true,            // Create Facebook OpenGraph image. `boolean`
            twitter: true,              // Create Twitter Summary Card image. `boolean`
            windows: true,              // Create Windows 8 tile icons. `boolean`
            yandex: false                // Create Yandex browser icon. `boolean`
        }
      }))
      .on("error", gutil.log)
      .pipe(this.gulp.dest(dest));

};
