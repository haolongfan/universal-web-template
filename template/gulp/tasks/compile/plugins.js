/* jshint node: true */

/*
  build:plugins Task
  Generate external javascript plugin files
*/

'use strict';

var uglify = require('gulp-uglify'),
    concat = require('gulp-concat'),
    plumber = require('gulp-plumber'),
    obfuscator = require('gulp-js-obfuscator');

module.exports = function() {

  var source = this.opts.config.script.plugins,
      dest = this.opts.config.compile.plugins;

  return this.gulp.src([
    this.opts.path.join(source, '**/*.js'),
  ])
  .pipe(plumber())
  .pipe(uglify({
    preserveComments: 'some'
  }))
  .pipe(obfuscator())
  .pipe(this.gulp.dest(dest));

};
