/* jshint node: true */

/*
  build:clean Task
  Clean all build directories and files
*/

'use strict';

var del = require('del');

module.exports = function() {

  return del([
    this.opts.config.compile.root
  ]);

};
