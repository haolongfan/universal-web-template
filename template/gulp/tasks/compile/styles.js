/* jshint node: true */

/*
  build:styles Task
  Generate css files
*/

'use strict';

var sass = require('gulp-sass'),
    sassGlob = require('gulp-sass-glob'),
    sourcemaps = require('gulp-sourcemaps'),
    autoprefixer = require('gulp-autoprefixer'),
    concat = require('gulp-concat'),
    plumber = require('gulp-plumber');

module.exports = function() {

  var source = this.opts.config.source.sass,
      dest = this.opts.config.compile.styles;

  this.gulp.src(this.opts.vendors.styles, {
    style: 'compressed'
  })
  .pipe(plumber())
  .pipe(sourcemaps.init())
  .pipe(concat('vendors.css'))
  .pipe(sourcemaps.write('/'))
  .pipe(this.gulp.dest(dest));

  return this.gulp.src(
      this.opts.path.join(source, '/**/*.scss'), {
          style: 'compressed'
      }
    )
    .pipe(plumber())
    .pipe(sourcemaps.init())
    .pipe(sassGlob())
    .pipe(sass({
      errLogToConsole: true
    }))
    .pipe(autoprefixer({
      browsers: ['last 2 versions'],
      cascade: false
    }))
    .pipe(sourcemaps.write('/'))
    .pipe(this.gulp.dest(dest))
    .pipe(this.opts.browserSync.stream());
};
