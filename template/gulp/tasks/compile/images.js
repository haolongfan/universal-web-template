/* jshint node: true */

/*
  build:images Task
  Copy data files into build directory
*/

'use strict';

var plumber = require('gulp-plumber');

module.exports = function() {

  var source = this.opts.config.source.images,
      dest = this.opts.config.compile.images;

  return this.gulp.src([
    this.opts.path.join(source, this.opts.config.filetype.images)
  ])
  .pipe(plumber())
  .pipe(this.gulp.dest(dest));

};
