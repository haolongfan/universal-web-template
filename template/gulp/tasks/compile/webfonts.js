/* jshint node: true */

/*
  build:webfonts Task
  Generate web Fonts
  Install font packages: brew install fontforge ttfautohint ttf2eot batik
*/

'use strict';

var gutil = require('gulp-util'),
    flatten = require('gulp-flatten'),
    fontgenerator = require('gulp-fontgen'),
    plumber = require('gulp-plumber');

module.exports = function() {

  var source = this.opts.config.source.fonts,
      dest = this.opts.config.compile.fonts;

  this.gulp.src([
      this.opts.path.join(source, '/**/*.{ttf,otf}')
    ])
    .pipe(plumber())
    .pipe(fontgenerator({
      dest: dest
    }));

  return this.gulp.src(this.opts.vendors.fonts.concat([
      this.opts.path.join(source, this.opts.config.filetype.fonts)
    ]))
    .on('error', gutil.log)
    .pipe(flatten())
    .pipe(this.gulp.dest(dest));

};
