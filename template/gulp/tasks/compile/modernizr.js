/* jshint node: true */

/*
  build:modernizr Task
  Generate modernizr javascript file
*/

'use strict';

var modernizr = require('gulp-modernizr'),
    uglify = require('gulp-uglify'),
    plumber = require('gulp-plumber');

module.exports = function() {

  var source = this.opts.config.script.custom,
      dest = this.opts.config.compile.scripts;

  return this.gulp.src([
      this.opts.path.join(source, '**/*.js'),
    ])
    .pipe(plumber())
    .pipe(modernizr('modernizr.js', {
      options: [
        'setClasses'
      ],
      tests: [
        'touchevents'
      ]
    }))
    .pipe(uglify())
    .pipe(this.gulp.dest(dest));

};
