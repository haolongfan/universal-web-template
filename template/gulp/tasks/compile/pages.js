/* jshint node: true */

/*
  build:pages Task
  Generate html pages
*/

'use strict';

var hbsmaster = require('gulp-handlebars-master'),
    rename = require('gulp-rename'),
    plumber = require('gulp-plumber'),
    glob = require('require-glob');

module.exports = function() {

  var pagePath = './src/pages',
      source = this.opts.config.source.pages,
      helper = this.opts.config.helpers,
      dest = this.opts.config.compile.root,
      path = this.opts.path;

  // load page data
  var data = glob.sync(source + '/data/*.json', {
    bustCache: true
  });
  data.app = this.opts.pkg.app;

  // get page helpers
  var helpers = glob.sync(helper + '/*.js');

  // copy static html pages
  this.gulp.src([
    path.join(source, '/static/**/*.html'),
  ])
  .pipe(this.gulp.dest(dest));

  // generate html pages
  return this.gulp.src([
    path.join(source, '/pages/*.hbs')
  ])
  .pipe(plumber())
  .pipe(hbsmaster(source + '/master.hbs',
    data, {
      batch: [pagePath + '/partials', pagePath + '/components'],
      helpers: helpers
    }
  ))
  .pipe(rename(function(path) {
    path.extname = '.html';
  }))
  .pipe(this.gulp.dest(dest));

};
