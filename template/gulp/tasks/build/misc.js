/* jshint node: true */

/*
  deploy:misc Task
  Copy misc files into deploy directory
*/

'use strict';

module.exports = function() {

  var source = this.opts.config.source.misc.build,
      dest = this.opts.config.build.root;

  return this.gulp.src([
    this.opts.path.join(source, '**/*')
  ])
  .pipe(this.gulp.dest(dest));

};
