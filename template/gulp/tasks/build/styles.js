/* jshint node: true */

/*
  deploy:styles Task
  Copy and minify css files into deploy directory
*/

'use strict';

var cleanCSS = require('gulp-clean-css'),
    size = require('gulp-size');

module.exports = function() {

  var source = this.opts.config.compile.styles,
      dest = this.opts.config.build.styles;

  return this.gulp.src([
    this.opts.path.join(source, '/*.css'),
  ])
  .pipe(cleanCSS({
    keepSpecialComments: 0
  }))
  .pipe(size({
    showFiles: true,
    title: 'css file -'
  }))
  .pipe(this.gulp.dest(dest));

};
