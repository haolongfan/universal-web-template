/* jshint node: true */

/*
  deploy:favicons Task
  Copy favicons files into deploy directory
*/

'use strict';

module.exports = function() {

  var source = this.opts.config.compile.root,
      dest = this.opts.config.build.root;

  return this.gulp.src([
    this.opts.path.join(source, this.opts.config.filetype.favicons)
  ])
  .pipe(this.gulp.dest(dest));

};
