/* jshint node: true */

/*
  deploy:fonts Task
  Copy font files into deploy directory
*/

'use strict';

module.exports = function() {

  var source = this.opts.config.compile.fonts,
      dest = this.opts.config.build.fonts;

  return this.gulp.src([
    this.opts.path.join(source, this.opts.config.filetype.fonts)
  ])
  .pipe(this.gulp.dest(dest));

};
