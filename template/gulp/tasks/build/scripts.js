/* jshint node: true */

/*
  deploy:scripts Task
  Copy and minify javascript files into deploy directory
*/

'use strict';

var uglify = require('gulp-uglify'),
    size = require('gulp-size');

module.exports = function() {

  var source = this.opts.config.compile.scripts,
      dest = this.opts.config.build.scripts;

  return this.gulp.src([
    this.opts.path.join(source, '**/*.js'),
  ])
  .pipe(uglify())
  .pipe(size({
    showFiles: true,
    title: 'js file -'
  }))
  .pipe(this.gulp.dest(dest));

};
