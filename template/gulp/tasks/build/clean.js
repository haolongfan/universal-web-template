/* jshint node: true */

/*
  deploy:clean Task
  Clean all public directories and files
*/

'use strict';

var del = require('del');

module.exports = function() {
  return del([
    this.opts.config.build.root
  ]);
};
