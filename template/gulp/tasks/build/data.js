/* jshint node: true */

/*
  deploy:data Task
  Copy data files into deploy directory
*/

'use strict';

module.exports = function() {

  var source = this.opts.config.compile.data,
      dest = this.opts.config.build.data;

  return this.gulp.src([
    this.opts.path.join(source, '**/*')
  ])
  .pipe(this.gulp.dest(dest));

};
