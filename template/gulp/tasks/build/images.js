/* jshint node: true */

/*
  deploy:images Task
  Copy and optimize image files into deploy directory
*/

'use strict';

var imagemin = require('gulp-imagemin');

module.exports = function() {

  var source = this.opts.config.compile.images,
      dest = this.opts.config.build.images;

  return this.gulp.src([
    this.opts.path.join(source, this.opts.config.filetype.images)
  ])
  .pipe(imagemin())
  .pipe(this.gulp.dest(dest));

};
