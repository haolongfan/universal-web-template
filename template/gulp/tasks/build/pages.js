/* jshint node: true */

/*
  deploy:pages Task
  Copy and minify html page files into deploy directory
*/

'use strict';

var minifyHTML = require('gulp-htmlmin'),
    size = require('gulp-size');

module.exports = function() {

  var source = this.opts.config.compile.root,
      dest = this.opts.config.build.root;

  return this.gulp.src([
    this.opts.path.join(source, '**/*.html'),
  ])
  .pipe(minifyHTML())
  .pipe(size({
    showFiles: true,
    title: 'html file -'
  }))
  .pipe(this.gulp.dest(dest));

};
