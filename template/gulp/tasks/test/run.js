/* jshint node: true */

/*
  test:run Task
  Run unit test scripts
*/

'use strict';

var qunit = require('gulp-qunit'),
    plumber = require('gulp-plumber');

module.exports = function() {

    return this.gulp.src([
      this.opts.path.join(this.opts.config.compile.root, '/**/*.html')
    ])
    .pipe(plumber())
    .pipe(qunit());

};
