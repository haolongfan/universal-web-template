/* jshint node: true */

/*
  test:audit Task
  Audit script code
*/

'use strict';

var webstandards = require('gulp-webstandards'),
    plumber = require('gulp-plumber');

module.exports = function() {

  return this.gulp.src([
    this.opts.path.join(this.opts.config.compile.root, '/**/*'),
  ])
  .pipe(plumber())
  .pipe(webstandards());

};
