/* jshint node: true */

/*
  test:pages Task
  Generate testing pages
*/

'use strict';

var rename = require('gulp-rename'),
    hbsmaster = require('gulp-handlebars-master'),
    plumber = require('gulp-plumber');

module.exports = function() {

    var pagePath = './src/pages';

    var data = require(this.opts.config.source.pages + '/data.json');
    var helpers = {
      json: require(this.opts.config.helpers + '/json.js'),
      iff: require(this.opts.config.helpers + '/iff.js'),
      isnavactive: require(this.opts.config.helpers + '/isnavactive.js')
    };

    data.app = this.opts.pkg.app;
    data.test_enabled = true;

    return this.gulp.src(this.opts.config.source.pages + '/pages/*.hbs')
    .pipe(plumber())
    .pipe(hbsmaster(this.opts.config.source.pages + '/master.hbs',
      data, {
        batch: [pagePath + '/partials', pagePath + '/components', pagePath + '/tests'],
        helpers: helpers
      }
    ))
    .pipe(rename(function(path) {
      path.extname = '.html';
    }))
    .pipe(this.gulp.dest(this.opts.config.compile.root));

};
