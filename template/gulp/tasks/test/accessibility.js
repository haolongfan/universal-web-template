/* jshint node: true */

/*
  test:accessibility Task
  Audit script code
*/

'use strict';

var accessibility = require('gulp-accessibility'),
    rename = require('gulp-rename'),
    plumber = require('gulp-plumber');

module.exports = function() {

  return this.gulp.src([
    this.opts.path.join(this.opts.config.compile.root, '/**/*.html'),
  ])
  .pipe(plumber())
  .pipe(accessibility({
    force: true
  }))
  .pipe(accessibility.report({reportType: 'json'}))
  .pipe(rename({
    extname: '.json'
  }))
  .pipe(this.gulp.dest(this.opts.config.compile.reports));

};
