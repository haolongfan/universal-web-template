/* jshint node: true */

/*
  test:lint Task
  Validate script code
*/

'use strict';

var eslint = require('gulp-eslint'),
    plumber = require('gulp-plumber');

module.exports = function() {

  return this.gulp.src([
      this.opts.path.join(this.opts.config.script.custom, '/**/*.js'),
      this.opts.path.join(this.opts.config.script.react, '/**/*.js')
    ])
    .pipe(plumber())
    .pipe(eslint())
    .pipe(eslint.format())
    .pipe(eslint.failAfterError());

};
