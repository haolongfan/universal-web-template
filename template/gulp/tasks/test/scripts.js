/* jshint node: true */

/*
  test:scripts Task
  Build test scripts
*/

'use strict';

var concat = require('gulp-concat'),
    uglify = require('gulp-uglify'),
    plumber = require('gulp-plumber');

module.exports = function() {

  return this.gulp.src([
    this.opts.path.join(this.opts.config.script.tests, '/**/*.js')
  ])
  .pipe(plumber())
  .pipe(concat('tests.js'))
  .pipe(uglify())
  .pipe(this.gulp.dest(this.opts.config.compile.js));

};
