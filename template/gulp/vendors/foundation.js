/* jshint node: true */

/*
  Vendor - foundation
*/

'use strict';

module.exports = function(moduleDir) {

  return {
    styles:[
      moduleDir + '/foundation-sites/dist/foundation.css'
    ],
    plugins: [
      moduleDir + '/foundation-sites/dist/foundation.js'
    ]
  };

};
