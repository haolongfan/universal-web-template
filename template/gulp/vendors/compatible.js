/* jshint node: true */

/*
  Vendor - basic plugins
*/

'use strict';

module.exports = function(moduleDir) {

  return {
    plugins: [
      moduleDir + "/babel-polyfill/dist/polyfill.js",
      moduleDir + '/html5shiv/dist/html5shiv.js'
    ],
    styles:[
      moduleDir + '/normalize.css/normalize.css'
    ]
  };

};
