/* jshint node: true */

/*
  Vendor - bootstrap
*/

'use strict';

module.exports = function(moduleDir) {

  return {
    plugins: [
      moduleDir + '/bootstrap/dist/js/bootstrap.js'
    ],
    styles:[
      moduleDir + '/bootstrap/dist/css/bootstrap.css'
    ]
  };

};
