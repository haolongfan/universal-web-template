/* jshint node: true */

/*
  Vendor - jQuery
*/

'use strict';

module.exports = function(moduleDir) {

  return {
    plugins:[
      moduleDir + "/jquery/dist/jquery.js",
      moduleDir + "/jquery.easing/jquery.easing.js",
      moduleDir + "/jquery-validation/dist/jquery.validate.js"
    ]
  };

};
