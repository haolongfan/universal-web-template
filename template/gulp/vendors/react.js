/* jshint node: true */

/*
  Vendor - reactjs
*/

'use strict';

module.exports = function(moduleDir) {

  return {
    plugins:[
      moduleDir + "/react/dist/react.js",
      moduleDir + "/react-dom/dist/react-dom.js",
      moduleDir + "/history/umd/history.js",
      moduleDir + "/react-router/umd/ReactRouter.js",
      moduleDir + "/alt/dist/alt.js"
    ]
  };

};
