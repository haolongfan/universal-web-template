/* jshint node: true */

/*
  Vendor - bootstrap datepicker
*/

'use strict';

module.exports = function(moduleDir) {

  return {
    plugins: [
      moduleDir + '/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js'
    ],
    styles:[
      moduleDir + '/bootstrap-datepicker/dist/css/bootstrap-datepicker.css'
    ]
  };

};
