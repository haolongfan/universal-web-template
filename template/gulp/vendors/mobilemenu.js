/* jshint node: true */

/*
  Vendor - jquery.mmenu
*/

'use strict';

module.exports = function(moduleDir) {

  return {
    plugins: [
      moduleDir + '/jquery.mmenu/dist/js/jquery.mmenu.all.min.js'
    ],
    styles:[
      moduleDir + '/jquery.mmenu/dist/css/jquery.mmenu.all.css'
    ]
  };

};
