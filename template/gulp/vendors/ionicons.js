/* jshint node: true */

/*
  Vendor - ionicons
*/

'use strict';

module.exports = function(moduleDir) {

  return {
    styles:[
      moduleDir + '/ionicons-npm/css/ionicons.css'
    ],
    fonts: [
      moduleDir + '/ionicons-npm/fonts/*.{eot,svg,ttf,woff,woff2}'
    ]
  };
  
};
