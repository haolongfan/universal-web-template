/* jshint node: true */

/*
  Vendor - fontawesome
*/

'use strict';

module.exports = function(moduleDir) {

  return {
    styles:[
      moduleDir + '/font-awesome/css/font-awesome.css'
    ],
    fonts: [
      moduleDir + '/font-awesome/fonts/*.{eot,svg,ttf,woff,woff2}'
    ]
  };

};
