/* jshint node: true */

/*
  Vendor - Hammerjs
*/

'use strict';

module.exports = function(moduleDir) {

  return {
    plugins:[
      moduleDir + "/hammerjs/hammer.min.js"
    ]
  };

};
