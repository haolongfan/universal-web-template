/* jshint node: true */

/*
  Vendor - reactjs
*/

'use strict';

module.exports = function(moduleDir) {

  return {
    plugins:[
      moduleDir + "/retinajs/dist/retina.min.js"
    ]
  };

};
