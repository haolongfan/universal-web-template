/* jshint node: true */

/*
  Vendor - fancybox
*/

'use strict';

module.exports = function(moduleDir) {

  return {
    plugins:[
      moduleDir + "/fancybox/dist/js/jquery.fancybox.pack.js",
      moduleDir + "/fancybox/dist/helpers/js/jquery.fancybox-media.js"
    ]
  };

};
