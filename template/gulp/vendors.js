/* jshint node: true */

'use strict';

var path = require('path'),
    pkg = require('../package.json');

var moduleDir = path.resolve(__dirname, '../node_modules'),
    vendorDir = path.resolve(__dirname, '../gulp/vendors');
var modules = pkg.modules;

var vendors = {
  plugins: [],
  styles: [],
  fonts: []
};

for (var k in modules){

  if(modules[k]) {

    var m = require(vendorDir + '/' + k + '.js')(moduleDir);

    if(m.plugins) {
      vendors.plugins = vendors.plugins.concat(m.plugins);
    }

    if(m.styles) {
      vendors.styles = vendors.styles.concat(m.styles);
    }

    if(m.fonts) {
      vendors.fonts = vendors.fonts.concat(m.fonts);
    }

  }

}

module.exports = vendors;
