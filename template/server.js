'use strict';

var express = require('express'),
    app = express(),
    router = express.Router(),
    fs = require('fs'),
    bodyParser = require('body-parser');

var pkg = require(__dirname + '/package.json'),
    port = pkg.server.site_port,
    viewPath = __dirname + pkg.path.compile.root,
    controllerPath = __dirname + '/app/controllers';


app.use(express.static(viewPath));
app.set('view engine', 'gulp-handlebars-master');
app.set('views', viewPath);

app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json()); // this is used for parsing the JSON object from POST

// dynamically include controller routes
fs.readdirSync(controllerPath).forEach(function (file) {
  if(file.substr(-3) == '.js') {
      var route = require(controllerPath + "/" + file);
      route.controller(app);
  }
});

app.listen(port, function() {
  console.log('Server listening on port ' + port);
});
