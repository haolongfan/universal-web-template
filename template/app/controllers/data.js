'use strict';

var fs = require('fs');

module.exports.controller = function(app) {

  var dataPath = app.get('views') + "/data/";

  // dynamically generate data apis
  fs.readdirSync(dataPath).forEach(function (file) {
    var path = dataPath + file;

    var db = require("../models/db.js")(path);
    var data = db.getData("/");
    var filename = file.replace(/\.[^/.]+$/, "");

    /**
    * GET /api API
    */
    app.get('/data/' + filename, function(req, res) {
        res.end(JSON.stringify(data));
    });
  });

}
