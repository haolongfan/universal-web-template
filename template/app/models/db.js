'use strict';

var JsonDB = require('node-json-db');

module.exports = function(file) {

  var db = new JsonDB(file, true, false);

  return db;

};
