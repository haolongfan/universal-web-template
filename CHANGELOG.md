## Universal Web Template Change Log

All notable changes to this project will be documented in this file.


### [v2.0.1] - 2016-11-3
- 


### [v2.0.0] - 2016-9-30
- Upgrade Gulp tasks, improve browser synchronization process
- Update default template theme
- Introduce some third party plugins

### [v1.9.0] - 2016-8-18
- Add 'math' and 'truncate' handlebarsjs helpers
- Optimize browserSync and code watch
- Add 'accessibility' test
- Add 'bootstrap datepicker', 'mobilemenu' and 'retinajs' Support
- Upgrade NPM modules
- Update base theme
- Adjust Gulp tasks

### [v1.8.0] - 2016-8-1
- Remove Yeoman generator from this template
- Fix some urgent issues
- Update for template description

### [v1.7.0] - 2016-7-15

- Re-organize Gulp tasks
- Support browser reload when code has been changed
- Modularize plugins, styling and fonts
- Introduce Node Server
- Support Local JsonDB

### [v1.6.0] - 2016-6-23

- Update jQuery to version 3
- Support React Router
- Introduce Alt.js flux framework for complex react project
- Upgrade npm packages

### [v1.5.0] - 2016-6-7

- Support Qunit Testing and in-page testing
- Support Favicons Generation
- Improve theme supports
- Able to enable/disable modules
- Support flexible react rendering

### [v1.4.0] - 2016-5-2

- Support Test
- Simplify installation
- Support Web Font Generation

### [v1.3.0] - 2016-2-3

- Support Git
- Add Publish task
- Improve project installation

### [v1.2.0] - 2016-2-1

- Update template as a Yeoman generator
- Introduce electron app to generate OS executables

### [v1.0.0] - 2016-1-21

- Create a font-end template
- Add handlebars HTML template
- Introduce ReactJs into script
- Create a series of Gulp tasks
