[![Build status][ci-image]][ci-url]

# Universal Web Template
An universal web project template - let you quickly set up a project using Handlebars, sass and ReactJS for front-end templating. It can be adopted with most modern CMS.

## Directory Layout

```
.
├── /app/                       # NodeJS middleware, including data model, models and controllers
│   ├── /controllers/           # NodeJS Controllers
│   ├── /models/                # NodeJS Models
├── /build/                    # The folder for template build output
├── /dist/                     # The folder for compiled output
├── /gulp/                      # All gulp tasks
│   ├── /helpers/               # Handlebars Template Helpers
│   ├── /tasks/                 # All gulp tasks
│   ├── /vendors/               # Configuration of vendor modules
├── /node_modules/              # 3rd-party libraries and utilities
├── /package/                   # The folder for app package
├── /public/                       # The folder for CMS project
├── /src/                       # The source code of the application
│   ├── /favicons/              # All favicons generated
│   ├── /fonts/                 # TTF/OTF font files
│   ├── /images/                # All images for the template
│   ├── /misc/                  # Some files which will be deployed to the root of CMS
│   ├── /pages/                 # HTML template files and data
│       ├── /components/        # All template components
│       ├── /data/              # JSON data for each page and main components
│       ├── /pages/             # All pages
│       ├── /partials/          # Page partials
│       ├── /master.hbs         # Master Template
│   ├── /sass/                  # Sass files
│   ├── /scripts/               # Javascript files and ReactJs components
│       ├── /custom/            # Custom Javascript code
│       ├── /plugins/           # External Javascript plugin for accessing internal data
│       ├── /react/             # ReactJs components
│       ├── /tests/             # Unit Test code
│       ├── /vendors/           # Other javascript vendors
│── gulpfile.js                 # A list of core gulp tasks
│── server.js                   # NodeJs server
│── package.json                # The list of 3rd party libraries and utilities
```

## Features

### HTML
- [Handlebars Javascript Template](http://handlebarsjs.com/) is using HTML with JavaScript-based features embedded
- Minify HTML code with [Gulp Htmlmin](https://www.npmjs.com/package/gulp-htmlmin)
- Support to generate static HTML pages

### CSS
- Use [Sass](http://sass-lang.com/) CSS extension language
- Map compiled CSS to source stylesheets with source maps with [Gulp Sourcemaps](https://www.npmjs.com/package/gulp-sourcemaps)
- CSS Autoprefixing using [Gulp Autoprefixer](https://www.npmjs.com/package/gulp-autoprefixer)
- Minify CSS file with [Gulp Cssnano](https://www.npmjs.com/package/gulp-cssnano)
- Support Bootstrap, flexbox, Reponsive Design
- Allow to create multiple css themes

### Javascript
- [ReactJS](https://facebook.github.io/react/) provides a flexible and efficient view coming with [Gulp React Render](https://www.npmjs.com/package/gulp-react-render)
- Flexible ReactJS rendering via React JSON array and React components
- Process javascript file with ESLint using [Gulp Eslint](https://www.npmjs.com/package/gulp-eslint)
- Generate custom Modernizr builds to improve browser compatibility with [Gulp Modernizr](https://www.npmjs.com/package/gulp-modernizr)  
- Support next generation Javascript ES6 code with [Gulp Babel](https://www.npmjs.com/package/gulp-babel)
- Obfuscate project plugins with [Gulp Obfuscator](https://www.npmjs.com/package/gulp-obfuscator)
- Support to build project plugins for external sites to import

### Font
- Generate web fonts from TTF/OTF font files using [Gulp Fontgen](https://www.npmjs.com/package/gulp-fontgen)
- Create icon fonts from SVG icons with [Gulp Iconfont](https://www.npmjs.com/package/gulp-iconfont) and generate css for icon fonts with [Gulp Iconfont Css](https://www.npmjs.com/package/gulp-iconfont-css)
- Provide scss template for custom font

### Favicons
- Generate a range of favicons for web site, android, apple, windows and social shares with [Gulp Favicons](https://www.npmjs.com/package/gulp-favicons)

### Images
- Minify and optimize PNG, JPEG and GIF images with [Gulp Image Optimization](https://www.npmjs.com/package/gulp-image-optimization)

### Audit & Testing
- Support to run audit on the project via [Gulp Webstandards](https://www.npmjs.com/package/gulp-webstandards)
- Able to run Qunit test on pages. Through setting **test_enabled** as true, Qunit will run on each page.

### NodeJs Server & Middleware
- Run NodeJS server to set up backend middleware [Gulp Nodemon](https://www.npmjs.com/package/gulp-nodemon)
- Database using JSON file as storage for Node.JS [Node Json DB](https://www.npmjs.com/package/node-json-db)

### Building System
- Support to enable/disable each module from **package.json** configuration file.
- Using [Gulp](http://gulpjs.com/) streaming building system to render CSS, javascript and HTML pages. Configuration file is also provided, as you can update settings via the **config.js** file.
- Live server to run template and support browser syncing [Browser Sync](https://www.npmjs.com/package/browser-sync)
- Build cross platform desktop apps with web technologies using [Electron](http://electron.atom.io/)
- Place miscellaneous items in **src/misc**. e.g. .htaccess, web configure and Docker files. They will be copied to release.


## Getting Started

### Requirements
- Mac OS X, or Linux
- [Node.js](https://nodejs.org/) v5.0 or newer
- `npm` v3.3 or newer (new to [npm](https://docs.npmjs.com/)?)

### 1. Get the template via NPM
You can start the latest version of Universal Web Template on your local machine by running:

```shell
sudo npm install universal-web-template
```

or
```shell
git clone https://bitbucket.org/haolongfan/universal-web-template.git
```

Please grab the template from `/template` folder and copy to your project.


### 2. Install components via NPM

```shell
$ npm install -g npm-check-updates
$ brew install fontforge ttfautohint ttf2eot batik
$ sudo npm install
```


### 3. Run Template

```shell
$ npm start
```

This command will build the app from the source files (`/src`) into the dev output `/dist` folder. As soon as the initial build completes, it will start the Node.js server (`node build/server.js`) and [Browsersync](https://browsersync.io/) on top of it.

Now you can see your web app in a browser, on mobile devices and start hacking. Whenever you modify any of the source files inside the `/src` folder, it will recompile the app on the fly and refresh all the connected browsers.


### 4. Build Template

```shell
$ gulp
```

or

```shell
$ gulp compile
```

These two command will build the app from the source files (`/src`) into the dev output `/dist` folder.

### 5. Build Template

```shell
$ gulp
```

or

```shell
$ gulp build
```

These two command will build the app from the source files (`/src`) into the build output `/build` folder.


### 6. Integrate Template

```shell
$ gulp
```

or

```shell
$ gulp integrate
```

This command will generate final files to the public folder (`/public`) for CMS integration.

### 7. Watch Template

```shell
$ gulp watch
```

This command will watch any code change from the source files (`/src`) and re-build the app once modification happens.


### 8. Test Template

```shell
$ gulp test
```

This command will run Qunit testing based on test scripts which are generated from test script folder (`/src/scripts/tests`).  


### 9. Package Project as Executables Bundles

```shell
$ gulp pack
```

This command will generate OS-specific bundles (.app and .exe) for the web app. Supports building Windows, Linux or Mac executables.


### 10. Clean All Compiled Files

```shell
$ gulp clean
```

This command will clean all files from compile folder (`/dist`), build folder (`/build`), public folder (`/public`).



### How to Update
If you need to keep your project up to date with the recent changes, you can always run NPM update via:

```shell
$ npm update universal-web-template
```

If you want to update packages, please run:
```shell
$ sudo ncu -u
$ npm update
```

## Issues and New Features

If you found any issues or hope to add new features in, please record them in [here](https://bitbucket.org/haolongfan/universal-web-template/issues?status=new&status=open). I'll response you as soon as possible. Many thanks!


## License
Copyright (c) 2016 Haolong Fan Licensed under the MIT license.

[ci-image]: https://travis-ci.org/bahmutov/qunit-promises.png?branch=master
[ci-url]: https://bitbucket.org/haolongfan/universal-web-template
